module.exports = {
  filterByYearAndMonth: (year, month) => `AND(MONTH({order_placed}) = '${month}', YEAR({order_placed}) = '${year}')`,
};
