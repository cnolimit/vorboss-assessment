const mcache = require("memory-cache");
const Airtable = require("airtable");
const { filterByYearAndMonth } = require("./filters");

const base = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }).base(
  "app8wLQrrIMrnn673"
);

const roundNumber = (number, decimals) => {
  var newNumber = number.toFixed(parseInt(decimals));
  return parseFloat(newNumber);
};

var cache = (duration) => {
  return (req, res, next) => {
    let key = "__express__" + req.originalUrl || req.url;
    let cachedBody = mcache.get(key);

    if (cachedBody) {
      res.send(cachedBody);
      return;
    } else {
      res.sendResponse = res.send;
      res.send = (body) => {
        mcache.put(key, body, duration * 1000);
        res.sendResponse(body);
      };
      next();
    }
  };
};

const serialize = (order, total, ref) => {
  let data = total;

  if (ref) {
    if (!total[ref]) {
      total[ref] = { count: 0, total_price: 0, records: [] };
    }
    data = total[ref];
  }

  data.count += 1;
  data.total_price = roundNumber(data.total_price + order.fields.price, 2);
  data.records = [...data.records, order.fields];
};

const handleQuery = (query) => {
  const limit = query.limit || "";
  const filter = query.filterByYearAndMonth || "";
  let filterByFormula = "";

  if (filter) {
    const [year, month] = filter.split(":");
    filterByFormula = filterByYearAndMonth(year, month);
  }

  return { limit, filterByFormula };
};

module.exports = {
  cache,
  serialize,
  handleQuery,
  fetchOrders: (onEachCallback, onDoneCallback, selectArgs = {}) => {
    base("Orders")
      .select({
        view: "Grid view",
        sort: [{ field: "order_placed", direction: "desc" }],
        ...selectArgs,
      })
      .eachPage(
        function page(records, fetchNextPage) {
          records.forEach(function (record, idx) {
            onEachCallback(record);
          });

          fetchNextPage();
        },
        function done(err) {
          if (err) {
            console.error(err);
            onDoneCallback(err);
            return;
          }
          onDoneCallback();
        }
      );
  },
};
