require("dotenv").config();
const express = require("express");
const cors = require("cors");
const { fetchOrders, serialize, cache, handleQuery } = require("./helpers");

const app = express();
const port = 5000;
const CACHE_TIME = 120;

app.use(cors());
app.get("/", (req, res) => {
  res.send({
    title: "Hey",
    message: "Hello there welcome to the Puurfect API",
  });
});

app.get("/orders", cache(CACHE_TIME), (req, res) => {
  let orders = { count: 0, total_price: 0, records: [] };
  const { limit, filterByFormula } = handleQuery(req.query);

  fetchOrders(
    (record) => serialize(record, orders),
    () => res.send(orders),
    { maxRecords: parseInt(limit, 10), filterByFormula }
  );
});

app.get("/customers", cache(CACHE_TIME), (req, res) => {
  let customers = [];
  const { limit, filterByFormula } = handleQuery(req.query);

  fetchOrders(
    (record) => customers.push(record.fields),
    () => res.send(customers),
    {
      maxRecords: parseInt(limit, 10),
      fields: ["first_name", "last_name", "email"],
      filterByFormula,
    }
  );
});

app.get("/products", cache(CACHE_TIME), (req, res) => {
  let products = {};
  const { limit, filterByFormula } = handleQuery(req.query);

  fetchOrders(
    (record) => serialize(record, products, record.get("product_name")),
    () => res.send(products),
    {
      maxRecords: parseInt(limit, 10),
      fields: ["product_name", "price", "order_placed"],
      filterByFormula,
    }
  );
});

app.listen(port, () => console.log(`Example app listening on port ${port}`));
