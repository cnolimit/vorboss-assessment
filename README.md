<div align="center">
<h1>Vorboss - Frontend Assessment</h1>
<br />

<a href="https://user-images.githubusercontent.com/6928300/151963581-35d01312-3448-45e9-acf2-e6aec27e394c.png">
  <img
    width="1680"
    alt="Assessment screenshot"
    src="https://user-images.githubusercontent.com/6928300/151963581-35d01312-3448-45e9-acf2-e6aec27e394c.png"
  />
</a>

<br />
<br />

<p>This is a Vorboss assessment utilizing airtable API to create a statistical dashboard. </p>

<br />
</div>

<hr />

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- This project uses docker so you will need [docker](https://www.docker.com/products/docker-desktop) installed.
- The backend calls Airtable's API so you will need to add a `.env` file with the variable
- You will need [yarn](https://yarnpkg.com/) installed globally

```
AIRTABLE_API_KEY=[API_KEY]
```

#### Run Application Development

Install project dependencies to map node modules

```
yarn install
```

Start the services

```
make start
```

Once complete, you should have two services running

- The frontend application should be running on http://localhost:3000
- The backend server should be running on http://localhost:5000

### Project Description

This project utilizes the API provided in the Airtable docs in order to carry out the follow functions

- Display total Orders this month
- Display total Orders this year
- Display number of orders in progress
- Display total revenue
- Display a list of the most recent few orders

### Modules

| Module            | Why?                                                                                                                                                                        |
| :---------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| axios             | axios is really simple to use and allows you to manage create instances to better namespace and managed requests                                                            |
| date-fns          | date-fns is a light weight date utility library, 'light weight' because they export all their functions individually and allow you to only import the function you need.    |
| styled-components | styled-components helps simplify styling components and managing class names. It also uses a similar syntax to SCSS allowing for nesting styles, which is really convenient |
| dotenv            | dotenv allows us to load environment variables from a local .env file                                                                                                       |
| memory-cache      | memory-cache allows us to cache requests in express and serve them back for a faster response                                                                               |
| react-datepicker  | simple date picker that allows us to select year/month                                                                                                                      |

### Hardest Area

- Working with serializing the data into a format that would be most optimal for the frontend.

### Proudest Area

- Creating the UI components that made up the dashboard.
- Adding a blur loader

## What Would I Change

#### Frontend

- Style a custom Vorboss date picker component
- Add more pages to drill down on individual stats
- Update the layout to be more responsive
- Introduce storybook to better share, visualize and test the components
- Introduce client-side caching to improve loading times

#### Backend

- Create a better utility for handling filters

#### Infra

- Add testing and linting before building
- Create a production pipeline to serve dashboard files
