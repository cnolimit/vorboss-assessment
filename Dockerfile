FROM node:14 as development

WORKDIR /src

COPY yarn.lock .

COPY . .

RUN yarn install

EXPOSE 3000 5000