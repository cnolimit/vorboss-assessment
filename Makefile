start:
	@docker compose up

stop:
	@docker compose down

clean:
	@docker compose down -v

build:
	@docker compose build

test:
	@echo test

lint:
	@echo lint