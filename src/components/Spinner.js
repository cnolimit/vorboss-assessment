import styled, { keyframes } from "styled-components";

const spin = keyframes`
  to { -webkit-transform: rotate(360deg); }
`;

const Spinner = styled.div`
  display: inline-block;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  border: 3px solid #08173579;
  border-top-color: var(--main-bg-color);
  animation: ${spin} 1s ease-in-out infinite;
`;

export default Spinner;
