import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Card from "./Card";
import visitor from "../icons/visitor.svg";
import { capitalise } from "../helpers/utils";
import OverflowLoader from "./OverflowLoader";

const StyledCard = styled(Card)`
  width: ${({ fullWidth }) => (fullWidth ? "100%" : "300px")};
  max-width: 100%;
  height: fit-content;

  ul {
    max-height: 675px;
    overflow: auto;
  }
`;

const Heading = styled.h1`
  font-size: 1.3em;
  margin: 0;
  font-weight: 400;
  margin-bottom: 16px;
  font-weight: 400;
  color: var(--main-text-color);
`;

const ListItem = styled.div`
  display: flex;
  margin: 16px 0;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin-left: 16px;

  img {
    width: 50px;
  }
`;

const Name = styled.span`
  color: var(--main-text-color);
`;

const Email = styled.span`
  color: var(--sub-text-color);
`;

const CustomerList = ({ list, fullWidth, loading }) => {
  return (
    <OverflowLoader loading={loading}>
      <StyledCard fullWidth={fullWidth}>
        <Heading>Customer List</Heading>
        <ul>
          {list.map(({ first_name, last_name, email }) => (
            <ListItem key={email}>
              <img src={visitor} width="40px" alt="customer" />
              <Wrapper>
                <Name>{capitalise(`${first_name} ${last_name}`)}</Name>
                <Email>{email}</Email>
              </Wrapper>
            </ListItem>
          ))}
        </ul>
      </StyledCard>
    </OverflowLoader>
  );
};

CustomerList.propTypes = {
  list: PropTypes.array,
  fullWidth: PropTypes.bool,
  loading: PropTypes.bool,
};

export default CustomerList;
