import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import OverflowLoader from "./OverflowLoader";

const Container = styled.div`
  display: grid;
  overflow: hidden;
  border-radius: 16px;
  overflow-y: scroll;
  max-height: 750px;
  box-shadow: var(--card-shadow);
`;

const Header = styled.div`
  display: grid;
  height: 80px;
  padding: 0 24px;
  align-items: center;
  color: var(--base-white-color);
  background-color: var(--main-bg-color);

  h4 {
    font-size: 1.2em;
    margin: 0;
    margin-bottom: -16px;
  }
`;

const Headings = styled.div`
  display: grid;
  align-items: center;
  color: var(--base-white-color);
  grid-template-columns: ${({ columnCount }) => `repeat(${columnCount}, 1fr)`};
`;

const Values = styled(Header)`
  height: 65px;
  color: var(--main-text-color);
  background-color: var(--base-white-color);
  border-bottom: 1px solid #e6e8f0;
  grid-template-columns: ${({ columnCount }) => `repeat(${columnCount}, 1fr)`};
`;

const Wrapper = styled.div`
  overflow: auto;
`;

const DataTable = ({ children, headers, title, loading }) => {
  return (
    <OverflowLoader loading={loading}>
      <Container>
        <Header>
          <h4>{title}</h4>
          <Headings columnCount={headers.length}>
            {headers.map((heading) => (
              <span key={heading}>{heading}</span>
            ))}
          </Headings>
        </Header>
        <Wrapper>{children}</Wrapper>
      </Container>
    </OverflowLoader>
  );
};

DataTable.propTypes = {
  headers: PropTypes.array,
  title: PropTypes.string,
  loading: PropTypes.bool,
};

DataTable.Values = Values;

export default DataTable;
