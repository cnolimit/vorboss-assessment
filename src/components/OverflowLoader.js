import React from "react";
import styled from "styled-components";
import Spinner from "./Spinner";
const Container = styled.div`
  position: relative;
`;

const Blur = styled.div`
  ${({ loading }) => loading && "filter: blur(2px)"};
`;

const SpinnerContainer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
`;

const OverflowLoader = ({ loading, children }) => {
  return (
    <Container>
      {!loading && children}
      {loading && <Blur loading={loading}>{children}</Blur>}
      {loading && (
        <SpinnerContainer>
          <Spinner />
        </SpinnerContainer>
      )}
    </Container>
  );
};

export default OverflowLoader;
