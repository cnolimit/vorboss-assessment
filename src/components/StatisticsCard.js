import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import upArrow from "../icons/up_arrow.svg";
import downArrow from "../icons/down_arrow.svg";
import { numberWithCommas } from "../helpers/utils";
import Card from "./Card";
import OverflowLoader from "./OverflowLoader";

const Data = styled.div`
  font-size: 2em;
  color: var(--main-text-color);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const Title = styled.div`
  font-size: 1.1em;
  color: var(--sub-text-color);
  margin-bottom: 4px;
`;

const StatIcon = styled.img`
  border-radius: 50%;
  margin-bottom: 16px;
`;

const DiffPercentage = styled.div`
  display: flex;
  align-items: center;
  right: 22px;
  top: 22px;
  position: absolute;
  flex-direction: column;

  span {
    margin-left: -2px;
    color: ${({ decreased }) => (decreased ? "#FF754C" : "#0049c6")};
  }
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;

const PercentageText = styled.span`
  font-size: 0.5em;
`;

const getDiff = (current, previous) => {
  if (!current || !previous) {
    return;
  }
  const percent = Math.round((current / previous - 1) * 100);

  return {
    percent: percent.toString().replace("-", ""),
    decreased: percent < 0,
  };
};

const StatisticCard = ({ title, icon, data, loading }) => {
  let diff = {};

  if (data.compareValue) {
    diff = getDiff(data.value, data.compareValue);
  }

  return (
    <OverflowLoader loading={loading}>
      <Card>
        <StatIcon src={icon} width="50px" alt="" />
        <Title>{title}</Title>
        <Data>
          {data.type === "currency" && "£"}
          {numberWithCommas(data.value || 0)}
        </Data>
        {diff && data.compareValue && diff.percent !== "0" ? (
          <DiffPercentage decreased={diff.decreased}>
            <Wrapper>
              <img src={diff.decreased ? downArrow : upArrow} width="50%" alt="" />
              <span>{diff.percent}%</span>
            </Wrapper>
            <PercentageText>from last month</PercentageText>
          </DiffPercentage>
        ) : null}
      </Card>
    </OverflowLoader>
  );
};

StatisticCard.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  loading: PropTypes.bool,
  data: PropTypes.shape({
    value: PropTypes.number,
    compareValue: PropTypes.number,
    type: PropTypes.oneOf(["number", "currency"]),
  }),
};

export default StatisticCard;
