import React from "react";
import PropTypes from "prop-types";
import StatusBadge from "./StatusBadge";
import { format } from "date-fns";
import { numberWithCommas } from "../helpers/utils";
import DataTable from "./DataTable";

const OrderTable = ({ values, loading }) => {
  const headers = ["Order ID", "Date", "Total", "Status"];

  return (
    <DataTable title="Product Orders" headers={headers} loading={loading}>
      {values.map((data) => {
        return (
          <DataTable.Values key={data.order_id} columnCount={headers.length}>
            <span>#{data.order_id}</span>
            <span>{format(new Date(data.order_placed), "MMMM Mo yyyy")}</span>
            <span>£{numberWithCommas(data.price)}</span>
            <StatusBadge status={data.order_status} />
          </DataTable.Values>
        );
      })}
    </DataTable>
  );
};

OrderTable.propTypes = {
  values: PropTypes.array,
  loading: PropTypes.bool,
};

export default OrderTable;
