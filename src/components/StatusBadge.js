import React from "react";
import styled from "styled-components";
import { ORDER_STATUS } from "../constants/order";

const Container = styled.div`
  border-radius: 24px;
  padding: 8px;
  display: flex;
  max-width: 150px;
  align-items: center;
  justify-content: center;
  color: ${({ textColor }) => textColor};
  background-color: ${({ backgroundColor }) => backgroundColor};
`;

const StatusBadge = ({ status }) => {
  const { textColor, backgroundColor, text } = ORDER_STATUS[status];
  return (
    <Container textColor={textColor} backgroundColor={backgroundColor}>
      {text}
    </Container>
  );
};

export default StatusBadge;
