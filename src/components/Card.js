import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Container = styled.div`
  padding: 18px 24px 14px;
  border-radius: 24px;
  background-color: var(--base-white-color);
  position: relative;
  width: 230px;
  box-shadow: var(--card-shadow);
`;

const Card = ({ children, width, ...props }) => {
  return <Container {...props}>{children}</Container>;
};

Card.propTypes = {
  width: PropTypes.string,
};

export default Card;
