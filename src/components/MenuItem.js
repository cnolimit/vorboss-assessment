import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Container = styled.li`
  display: flex;
  padding: 16px 0;
  margin: 14px 0 0;
  align-items: center;
  border-radius: 12px;
  padding-left: 20px;
  color: ${({ active }) => (active ? "var(--base-white-color)" : "var(--sub-text-color)")};
  background-color: ${({ active }) => (active ? "var(--main-bg-color)" : "none")};
  transition: color 0.1s ease;

  &:hover {
    color: ${({ active }) => (active ? "var(--base-white-color)" : "var(--main-bg-color)")};
    cursor: pointer;
  }

  img {
    margin-right: 16px;
    fill: red;
  }
`;

const MenuItem = ({ children, active, icon, onClick }) => {
  return (
    <Container active={active} hasIcon={!!icon} onClick={onClick}>
      {icon && <img src={icon} width="12%" alt="" />}
      {children}
    </Container>
  );
};

MenuItem.propTypes = {
  icon: PropTypes.string,
  active: PropTypes.bool,
  onClick: PropTypes.func,
};

export default MenuItem;
