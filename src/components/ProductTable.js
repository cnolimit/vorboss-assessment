import React from "react";
import PropTypes from "prop-types";
import { capitalise, numberWithCommas } from "../helpers/utils";
import DataTable from "./DataTable";

const ProductTable = ({ values, loading }) => {
  const headers = ["Name", "Sales", "Amount"];

  return (
    <DataTable title="Product Sales" headers={headers} loading={loading}>
      {values.map((data) => {
        return (
          <DataTable.Values key={data.name} columnCount={headers.length}>
            <span>{capitalise(data.name)}</span>
            <span>{data.count}</span>
            <span>£{numberWithCommas(data.total_price)}</span>
          </DataTable.Values>
        );
      })}
    </DataTable>
  );
};

ProductTable.propTypes = {
  values: PropTypes.array,
  loading: PropTypes.bool,
};

export default ProductTable;
