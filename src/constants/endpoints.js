export const ENDPOINTS = {
  orders: "/orders",
  products: "/products",
  customers: "/customers",
};
