export const ORDER_STATUS = {
  in_progress: {
    name: "in_progress",
    textColor: "#FF754C",
    backgroundColor: "#FFF3DC",
    text: "Pending",
  },
  cancelled: {
    name: "cancelled",
    textColor: "#FF754C",
    backgroundColor: "#FFEBF6",
    text: "Cancelled",
  },
  placed: {
    name: "placed",
    textColor: "#244CB2",
    backgroundColor: "#CDF4FF",
    text: "Placed",
  },
  shipped: {
    name: "shipped",
    textColor: "#30B224",
    backgroundColor: "#DCFFCD",
    text: "Shipped",
  },
};
