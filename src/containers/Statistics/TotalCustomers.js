import React, { useEffect, useState } from "react";
import StatisticsCard from "../../components/StatisticsCard";
import { getCustomers } from "../../helpers/api";
import members from "../../icons/members.svg";

const TotalCustomers = () => {
  const [customerCount, setCustomerCount] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    getCustomers().then(({ data }) => {
      setCustomerCount(data.length);
      setLoading(false);
    });
  }, []);

  return (
    <StatisticsCard
      loading={loading}
      title="Total Customers"
      data={{ value: customerCount }}
      icon={members}
    />
  );
};

export default TotalCustomers;
