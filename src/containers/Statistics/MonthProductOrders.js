import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { getProducts } from "../../helpers/api";
import ProductTable from "../../components/ProductTable";

const MonthProductOrders = ({ year, month }) => {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    getProducts({ filterByYearAndMonth: `${year}:${month}` }).then(({ data }) => {
      const products = Object.entries(data).map(([key, value]) => ({
        ...value,
        name: key,
      }));
      setOrders(products);
      setLoading(false);
    });
  }, [year, month]);

  return <ProductTable loading={loading} values={orders} />;
};

MonthProductOrders.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default MonthProductOrders;
