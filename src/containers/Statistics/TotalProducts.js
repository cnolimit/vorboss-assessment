import React, { useEffect, useState } from "react";
import StatisticsCard from "../../components/StatisticsCard";
import { getProducts } from "../../helpers/api";
import members from "../../icons/members.svg";

const TotalProducts = () => {
  const [productCount, setProductCount] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    getProducts().then(({ data }) => {
      setProductCount(Object.values(data).length);
      setLoading(false);
    });
  }, []);

  return (
    <StatisticsCard
      loading={loading}
      title="Total Products"
      data={{ value: productCount }}
      icon={members}
    />
  );
};

export default TotalProducts;
