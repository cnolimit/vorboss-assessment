import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { getCustomers } from "../../helpers/api";
import CustomerList from "../../components/CustomerList";

const MonthCustomersList = ({ year, month }) => {
  const [customers, setCustomers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    getCustomers({ filterByYearAndMonth: `${year}:${month}` }).then(({ data }) => {
      setLoading(false);
      setCustomers(data);
    });
  }, [year, month]);

  return <CustomerList loading={loading} list={customers} />;
};

MonthCustomersList.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default MonthCustomersList;
