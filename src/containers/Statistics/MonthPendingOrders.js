import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import StatisticsCard from "../../components/StatisticsCard";
import { getOrders } from "../../helpers/api";
import order from "../../icons/order.svg";
import { ORDER_STATUS } from "../../constants/order";
import { getMonth, getYear, subMonths, subYears } from "date-fns";
import { filterByYearAndMonth } from "../../helpers/filters";

const MonthPendingOrders = ({ year, month }) => {
  const [pendingOrdersCount, setPendingOrdersCount] = useState(0);
  const [previousPendingOrdersCount, setPreviousPendingOrdersCount] = useState(0);
  const [loading, setLoading] = useState(true);

  const date = `${year}-${month}-1`;
  const monthDate = new Date(date);

  const filterByPendingStatus = (order) => order.order_status === ORDER_STATUS.in_progress.name;

  useEffect(() => {
    setLoading(true);
    getOrders({ filterByYearAndMonth: filterByYearAndMonth(year, month) })
      .then(({ data }) => {
        setPendingOrdersCount(data.records.filter(filterByPendingStatus).length);
      })
      .then(() => {
        const previousMonth = getMonth(subMonths(monthDate, 1)) + 1;
        const previousYear = month === 1 ? getYear(subYears(monthDate, 1)) : year;

        getOrders({ filterByYearAndMonth: filterByYearAndMonth(previousYear, previousMonth) }).then(({ data }) => {
          setPreviousPendingOrdersCount(data.records.filter(filterByPendingStatus).length);
          setLoading(false);
        });
      });
  }, [date]);
  return (
    <StatisticsCard
      loading={loading}
      title="Pending Orders"
      data={{ value: pendingOrdersCount, compareValue: previousPendingOrdersCount }}
      icon={order}
    />
  );
};

MonthPendingOrders.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default MonthPendingOrders;
