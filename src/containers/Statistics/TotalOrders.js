import React, { useEffect, useState } from "react";
import StatisticsCard from "../../components/StatisticsCard";
import { getOrders } from "../../helpers/api";
import order from "../../icons/order.svg";

const TotalOrders = () => {
  const [sales, setSales] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    getOrders().then(({ data }) => {
      setSales(data.records.length);
      setLoading(false);
    });
  }, []);

  return <StatisticsCard loading={loading} title="Total Orders" data={{ value: sales }} icon={order} />;
};

export default TotalOrders;
