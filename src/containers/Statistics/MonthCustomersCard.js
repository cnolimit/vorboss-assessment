import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import StatisticsCard from "../../components/StatisticsCard";
import { getCustomers } from "../../helpers/api";
import members from "../../icons/members.svg";
import { filterByYearAndMonth } from "../../helpers/filters";
import { getMonth, getYear, subMonths, subYears } from "date-fns";

const MonthCustomersCard = ({ year, month }) => {
  const [customerCount, setCustomerCount] = useState(0);
  const [previousCustomerCount, setPreviousCustomerCount] = useState(0);
  const [loading, setLoading] = useState(true);

  const date = `${year}-${month}-1`;
  const monthDate = new Date(date);

  useEffect(() => {
    setLoading(true);
    getCustomers({ filterByYearAndMonth: filterByYearAndMonth(year, month) })
      .then(({ data }) => {
        setCustomerCount(data.length);
      })
      .then(() => {
        const previousMonth = getMonth(subMonths(monthDate, 1)) + 1;
        const previousYear = month === 1 ? getYear(subYears(monthDate, 1)) : year;

        getCustomers({ filterByYearAndMonth: filterByYearAndMonth(previousYear, previousMonth) }).then(({ data }) => {
          setPreviousCustomerCount(data.length);
          setLoading(false);
        });
      });
  }, [date]);

  return (
    <StatisticsCard
      loading={loading}
      title="Customers"
      data={{ value: customerCount, compareValue: previousCustomerCount }}
      icon={members}
    />
  );
};

MonthCustomersCard.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default MonthCustomersCard;
