import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { getOrders } from "../../helpers/api";
import OrderTable from "../../components/OrderTable";

const MonthOrders = ({ year, month }) => {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    getOrders({ filterByYearAndMonth: `${year}:${month}` }).then(({ data }) => {
      setOrders(data.records);
      setLoading(false);
    });
  }, [year, month]);

  return <OrderTable loading={loading} values={orders} />;
};

MonthOrders.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default MonthOrders;
