import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import StatisticsCard from "../../components/StatisticsCard";
import { getProducts } from "../../helpers/api";
import products from "../../icons/products.svg";
import { filterByYearAndMonth } from "../../helpers/filters";
import { getMonth, getYear, subMonths, subYears } from "date-fns";

const MonthProductCount = ({ year, month }) => {
  const [sales, setSales] = useState(0);
  const [previousMonthSales, setPreviousMonthSales] = useState(0);
  const [loading, setLoading] = useState(true);

  const date = `${year}-${month}-1`;
  const monthDate = new Date(date);

  useEffect(() => {
    setLoading(true);
    getProducts({ filterByYearAndMonth: filterByYearAndMonth(year, month) })
      .then(({ data }) => {
        setSales(Object.values(data).length);
      })
      .then(() => {
        const previousMonth = getMonth(subMonths(monthDate, 1)) + 1;
        const previousYear = month === 1 ? getYear(subYears(monthDate, 1)) : year;

        getProducts({ filterByYearAndMonth: filterByYearAndMonth(previousYear, previousMonth) }).then(({ data }) => {
          setPreviousMonthSales(Object.values(data).length);
          setLoading(false);
        });
      });
  }, [date]);

  return (
    <StatisticsCard
      loading={loading}
      title="Products Sold"
      data={{ value: sales, compareValue: previousMonthSales }}
      icon={products}
    />
  );
};

MonthProductCount.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default MonthProductCount;
