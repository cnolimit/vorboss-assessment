import React, { useEffect, useState } from "react";
import StatisticsCard from "../../components/StatisticsCard";
import { getOrders } from "../../helpers/api";
import salesIcon from "../../icons/sales.svg";

const TotalSales = () => {
  const [sales, setSales] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    getOrders().then(({ data }) => {
      setSales(data.total_price);
      setLoading(false);
    });
  }, []);

  return (
    <StatisticsCard
      loading={loading}
      title="Total Revenue"
      data={{ value: sales, type: "currency" }}
      icon={salesIcon}
    />
  );
};

export default TotalSales;
