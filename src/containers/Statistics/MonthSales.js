import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import StatisticsCard from "../../components/StatisticsCard";
import { getOrders } from "../../helpers/api";
import income from "../../icons/income.svg";
import { filterByYearAndMonth } from "../../helpers/filters";
import { getMonth, getYear, subMonths, subYears } from "date-fns";

const MonthSales = ({ year, month }) => {
  const [sales, setSales] = useState(0);
  const [previousMonthSales, setPreviousMonthSales] = useState(0);
  const [loading, setLoading] = useState(true);

  const date = `${year}-${month}-1`;
  const monthDate = new Date(date);

  useEffect(() => {
    setLoading(true);
    getOrders({ filterByYearAndMonth: filterByYearAndMonth(year, month) })
      .then(({ data }) => {
        setSales(data.total_price);
      })
      .then(() => {
        const previousMonth = getMonth(subMonths(monthDate, 1)) + 1;
        const previousYear = month === 1 ? getYear(subYears(monthDate, 1)) : year;

        getOrders({ filterByYearAndMonth: filterByYearAndMonth(previousYear, previousMonth) }).then(({ data }) => {
          setPreviousMonthSales(data.total_price);
          setLoading(false);
        });
      });
  }, [date]);

  return (
    <StatisticsCard
      loading={loading}
      title="Sales"
      data={{ value: sales, type: "currency", compareValue: previousMonthSales }}
      icon={income}
    />
  );
};

MonthSales.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
};

export default MonthSales;
