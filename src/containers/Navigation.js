import React, { useState } from "react";
import styled from "styled-components";
import MenuItem from "../components/MenuItem";
import catLogo from "../images/cat_logo.svg";
import dashboardIcon from "../icons/dashboard.svg";
import { useNavigate } from "react-router-dom";

const PAGES = {
  dashboard: "/",
};

const Container = styled.div`
  border-right: 2px solid #e6e8f0;
  background-color: var(--base-white-color);
  padding: 60px 0;
`;

const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const LogoHeading = styled.h1`
  font-size: 1.2em;
  color: var(--main-bg-color);
  margin-top: 30px;
`;

const Menu = styled.ul`
  margin: 40px 20px 0;
`;

const Navigation = (props) => {
  const [currentPage, setCurrentPage] = useState(window.location.pathname);
  const navigate = useNavigate();

  const changePage = (page) => {
    setCurrentPage(page);
    navigate(page);
  };

  return (
    <Container>
      <LogoWrapper>
        <img src={catLogo} width="30%" alt="Cat face" />
        <LogoHeading>Puurfect Creations</LogoHeading>
      </LogoWrapper>
      <Menu>
        <MenuItem
          active={PAGES.dashboard === currentPage}
          icon={dashboardIcon}
          onClick={() => changePage(PAGES.dashboard)}
        >
          Dashboard
        </MenuItem>
      </Menu>
    </Container>
  );
};
export default Navigation;
