import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Navigation from "./Navigation";

const Container = styled.div`
  background: #fafbff;
  width: 100vw;
  height: 100vh;
  display: grid;
  grid-template-rows: 100%;
  grid-template-columns: 250px 1fr;
`;

const ContentContainer = styled.div`
  padding: 40px 50px;
  overflow: auto;
`;

const Heading = styled.h1`
  margin: 0;
  font-size: 4em;
  font-weight: 400;
  margin-left: -5px;
  margin-bottom: 20px;
  color: var(--main-text-color);
`;

const Layout = ({ children, heading }) => {
  return (
    <Container>
      <Navigation />
      <ContentContainer>
        <Heading>{heading}</Heading>
        {children}
      </ContentContainer>
    </Container>
  );
};

Layout.propTypes = {
  heading: PropTypes.string,
};

export default Layout;
