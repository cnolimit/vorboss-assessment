import React from "react";
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import Layout from "./containers/Layout";

const LayoutRoute = ({ Component, heading }) => {
  return (
    <Layout heading={heading}>
      <Component />
    </Layout>
  );
};

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<LayoutRoute Component={Dashboard} heading="Dashboard" />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </Router>
  );
}
