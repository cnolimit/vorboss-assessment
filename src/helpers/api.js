import axios from "axios";
import { ENDPOINTS } from "../constants/endpoints";

const apiDefaults = {
  timeout: 25000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
};

const api = axios.create({
  baseURL: `${process.env.REACT_APP_API_HOST}`,
  ...apiDefaults,
});

const createQuery = (data) => {
  const query = [];
  for (let d in data) query.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
  return query.join("&");
};

const getParams = (endpoint, params) => {
  if (!params) {
    return endpoint;
  }

  return `${endpoint}?${createQuery(params)}`;
};

export const getOrders = (params) => api.get(getParams(ENDPOINTS.orders, params));
export const getProducts = (params) => api.get(getParams(ENDPOINTS.products, params));
export const getCustomers = (params) => api.get(getParams(ENDPOINTS.customers, params));
