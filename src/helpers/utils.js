export const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
};

export const capitalise = (word) => word.replace(/(^\w{1})|(\s+\w{1})/g, (letter) => letter.toUpperCase());
