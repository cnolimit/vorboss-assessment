import React, { useState } from "react";
import { format, getMonth, getYear } from "date-fns";
import styled from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import MonthSales from "../containers/Statistics/MonthSales";
import MonthOrders from "../containers/Statistics/MonthOrders";
import MonthCustomersCard from "../containers/Statistics/MonthCustomersCard";
import MonthCustomersList from "../containers/Statistics/MonthCustomersList";
import MonthProductCount from "../containers/Statistics/MonthProductCount";
import MonthProductOrders from "../containers/Statistics/MonthProductOrders";
import TotalSales from "../containers/Statistics/TotalSales";
import TotalOrders from "../containers/Statistics/TotalOrders";
import TotalCustomers from "../containers/Statistics/TotalCustomers";
import MonthPendingOrders from "../containers/Statistics/MonthPendingOrders";
import TotalProducts from "../containers/Statistics/TotalProducts";

const Container = styled.div`
  display: flex;
  gap: 24px;
  flex-direction: column;
`;

const SmallCardWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 24px;
`;

const BigCardWrapper = styled.div`
  display: grid;
  gap: 24px;
  grid-template-rows: 1fr;
  grid-template-columns: 0.2fr 1fr;
`;

const MonthDateWrapper = styled.div``;
const TotalStatsWrapper = styled.div`
  margin-bottom: 50px;
`;

const Heading = styled.div`
  margin-bottom: 20px;
  h2 {
    font-size: 1.5em;
    font-weight: 400;
    margin: 0;
    color: var(--main-text-color);
  }

  span {
    display: block;
    font-size: 1em;
    color: var(--sub-text-color);
  }
`;

function Dashboard() {
  const [startDate, setStartDate] = useState(new Date());

  const month = getMonth(startDate) + 1;
  const year = getYear(startDate);

  return (
    <Container>
      <TotalStatsWrapper>
        <Heading>
          <h2>Total Summary</h2>
        </Heading>
        <SmallCardWrapper>
          <TotalSales />
          <TotalOrders />
          <TotalCustomers />
          <TotalProducts />
        </SmallCardWrapper>
      </TotalStatsWrapper>

      <MonthDateWrapper>
        <Heading>
          <h2>Monthly Summary</h2>
          <span>{format(startDate, "MMMM yyy")}</span>
        </Heading>
        <DatePicker
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          dateFormat="MM/yyyy"
          showMonthYearPicker
        />
      </MonthDateWrapper>

      <SmallCardWrapper>
        <MonthSales month={month} year={year} />
        <MonthProductCount month={month} year={year} />
        <MonthCustomersCard month={month} year={year} />
        <MonthPendingOrders month={month} year={year} />
      </SmallCardWrapper>

      <BigCardWrapper>
        <MonthCustomersList month={month} year={year} />
        <MonthOrders month={month} year={year} />
        <div style={{ gridColumn: "span 2" }}>
          <MonthProductOrders month={month} year={year} />
        </div>
      </BigCardWrapper>
    </Container>
  );
}

export default Dashboard;
